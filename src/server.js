import app from './app';

try {
  app.listen(3333, () => {
    console.log('Server listen on port 3333');
  });
} catch (e) {
  console.log(e.message);
  process.exit(1);
}
